class lighttpd::munin {
  lighttpd::config::file{'status':
    content => 'server.modules += ( "mod_status" )
$HTTP["remoteip"] == "127.0.0.1" {
  status.status-url = "/server-status"
  status.config-url = "/server-config"
}
',
  }
  munin::plugin::deploy{'lighttpd_':
    ensure  => absent,
    source  => 'lighttpd/munin/lighttpd_',
    require => Lighttpd::Config::File['status'],
  }
  munin::plugin{'lighttpd_total_accesses':
    ensure  => 'lighttpd_',
    require => Munin::Plugin::Deploy['lighttpd_'],
  }
  munin::plugin{'lighttpd_total_kbytes':
    ensure  => 'lighttpd_',
    require => Munin::Plugin::Deploy['lighttpd_'],
  }
  munin::plugin{'lighttpd_uptime':
    ensure  => 'lighttpd_',
    require => Munin::Plugin::Deploy['lighttpd_'],
  }
  munin::plugin{'lighttpd_busyservers':
    ensure  => 'lighttpd_',
    require => Munin::Plugin::Deploy['lighttpd_'],
  }
}
